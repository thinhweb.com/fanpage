<!-- BEGIN: main -->
<div class="nv-block-fanpage text-center">
	<div id="fb-root"></div>
	<script>
		( function(d, s, id) {
				var js,
				    fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id))
					return;
				js = d.createElement(s);
				js.id = id;
				js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
	</script>

	<div class="fb-page" data-href="{DATA.href}"
		<!-- BEGIN: width -->data-width="{DATA.width}"<!-- END: width -->
		<!-- BEGIN: height -->data-height="{DATA.height}"<!-- END: height -->
		<!-- BEGIN: tabs -->data-tabs="{DATA.tabs}"<!-- END: tabs -->
		<!-- BEGIN: cta -->data-hide-cta="{DATA.cta}"<!-- END: cta -->
		<!-- BEGIN: small_header -->data-small-header="{DATA.small_header}"<!-- END: small_header -->
		<!-- BEGIN: adapt_container_width-->data-adapt-container-width="{DATA.adapt_container_width}"<!-- END: adapt_container_width -->
		<!-- BEGIN: hide_cover -->data-hide-cover="{DATA.cover}"<!-- END: hide_cover -->
		<!-- BEGIN: show_facepile -->data-show-facepile="{DATA.facepile}"<!-- END: show_facepile --> >
		<div class="fb-xfbml-parse-ignore">
			<blockquote cite="{DATA.href}">
				<a href="{DATA.href}"><!-- BEGIN: name -->{DATA.name}<!-- END: name --></a>
			</blockquote>
		</div>
	</div>

</div>

<!-- END: main -->